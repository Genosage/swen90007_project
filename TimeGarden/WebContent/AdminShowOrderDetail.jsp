<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="<%=basePath%>">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/AdminPage.css">
</head>
<body>
<div class="background">
	<img src="images/02.jpg" width="1200px" height="800px"/>
</div>
<h1>Time Garden Administration Page</h1>
<div class="ShowTicket1">
<h3>Administrator Order Detail Page</h3>
</div>
<div class="ShowTicket">
<table>
	<tr>
		<td>order id</td>
		<td>${requestScope.orders.order_id}</td>
	</tr>
	<tr>
		<td>ticket id</td>
		<td>${requestScope.orders.ticket_id}</td>
	</tr>
	<tr>
		<td>ticket amounts&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>${requestScope.orders.ticket_number}</td>
	</tr>
	<tr>
		<td>order price</td>
		<td>${requestScope.orders.order_price}</td>
	</tr>
	<tr>
		<td>email</td>
		<td>${requestScope.orders.email}</td>
	</tr>
	<tr>
		<td>order status</td>
		<td>${requestScope.orders.order_status}</td>
	</tr>
	
</table>

<a href="showOrderDetail.do?flag=modify&order_id=${requestScope.orders.order_id}&ticket_id=${requestScope.orders.ticket_id} ">Modify it</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="deleteOrder.do?order_id=${requestScope.orders.order_id}">Delete it</a><br><br>
<a href="showOrderDetail.do?flag=modify&order_id=${requestScope.orders.order_id}&ticket_id=${requestScope.orders.ticket_id} ">Refuse it</a><br><br>
<a href="AdminIndex.jsp">Back to the main page</a>
</div>
</body>
</html>