<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="<%=basePath%>">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/ticketManagement.css">
</head>
<body>
<div class="background">
	<img src="images/02.jpg" width="1200px" height="800px"/>
</div>
<h1>Time Garden Administration Page</h1>

<div class="operation_list">
	<table>
		<tr>
			<td><a href="readAllUser.do">Read User Information</a><br><br></td>
		</tr>
		<tr>
			<td><a href="identifyPowerUser.do">Power User Management</a><br><br></td>
		</tr>
		
		<tr>
			<td><a href="addTag.do">User Interest Management</a></td>
		</tr>
		
	</table>
 
</div>


<div class="ticket_form2">
<table>
	<tr>
		<td>User id</td>
		<td>${requestScope.userDetail.user_id}</td>
	</tr>
	<tr>
		<td>email</td>
		<td>${requestScope.userDetail.email}</td>
	</tr>
	<tr>
		<td>password&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>${requestScope.userDetail.password}</td>
	</tr>
	<tr>
		<td>power</td>
		<td>${requestScope.userDetail.power}</td>
	</tr>
	<tr>
		<td>hour ticket number</td>
		<td>${requestScope.userDetail.hourticket_number}</td>
	</tr>
	<tr>
		<td>minute ticket number</td>
		<td>${requestScope.userDetail.minuteticket_number}</td>
	</tr>
	<tr>
		<td>tag</td>
		<td>${requestScope.userDetail.tag }</td>
	</tr>
	
</table>
<br><br> 
<a href="AdminIndex.jsp">Back to the main page</a>
</div>
</body>
</html>