<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="<%=basePath%>">

<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/ticketManagement.css">
</head>
<body>
<div class="background">
	<img src="images/02.jpg" width="1200px" height="800px"/>
</div>
<h1>Time Garden Administration Page</h1>

<div class="operation_list">
	<table>
		<tr>
			<td><a href="readAllUser.do">Read User Information</a><br><br></td>
		</tr>
		<tr>
			<td><a href="identifyPowerUser.do">Power User Management</a><br><br></td>
		</tr>
		
		<tr>
			<td><a href="addTag.do">User Interest Management</a><br><br></td>
		</tr>
		
		<tr>
			<td><a href="AdminManageUsers.jsp">Back</a></td>
		</tr>
		
	</table>
 
</div>
<div class="ticket_form3">
<c:forEach var="users" items="${requestScope.requests}">	

	<table>
	<tr>
		<td>email:&nbsp;&nbsp;&nbsp;${users.email }&nbsp;&nbsp;&nbsp;</td>
	</tr>
	<tr>		
		<td>Hour Park ticket number:&nbsp;</td>
		<td>${users.hourticket_number }&nbsp;&nbsp;</td>
		<td><a href="AdminAddTag.do?email=${users.email }&tag=Hour Park">Add Tag</a></td>
		
		<td>Minute Park ticket number:&nbsp;</td>
		<td>${users.minuteticket_number }&nbsp;&nbsp;</td>
		<td><a href="AdminAddTag.do?email=${users.email }&tag=Minute Park">Add Tag</a></td>
		
		<td>Second Park ticket number:&nbsp;</td>
		<td>${users.secondticket_number }&nbsp;&nbsp;</td>
		<td><a href="AdminAddTag.do?email=${users.email }&tag=Second Park">Add Tag</a></td>
	</tr>
</table>
<table>
<tr>
<td>-----------------------------------------------------------------------------
-------------------------------------------------------</td>
</tr>
</table>

</c:forEach><br><br>
<a href="AdminIndex.jsp">Back to the main page</a>
</div>
</body>
</html>