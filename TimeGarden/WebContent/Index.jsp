<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="<%=basePath%>">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body style="background:url(./images/01.jpg) no-repeat;">

<h1>Welcome to Time Garden</h1>
<h2>Welcome <a href="readOwnData.do">${sessionScope.user.email}</a> &nbsp;&nbsp;&nbsp;
<a href="exit.do">Log out</a>
</h2>

<div class="a1">
<a href="showTicket.do"> Show Ticket </a><br><br>
<a href="showAllOrder.do">Show Order</a><br><br>
<a href="checkAllRequest.do?flag=customerRead">Show Request</a><br><br>
<a href="showMessages.do">My own Message Box</a>
</div>
</body>
</html>