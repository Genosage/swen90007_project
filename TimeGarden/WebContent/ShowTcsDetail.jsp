<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="<%=basePath%>">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body style="background:url(./images/01.jpg) no-repeat;">
<h1>Time Garden</h1>
<div class="ShowTicket1">
<h3>Ticket Detail</h3>
</div>

<div class="ticket_pic">
<img src="images/${requestScope.tickets.image}" width="300px" height="200px"/>
<br><br>
<hr color="orange" width="650px" />

</div>

<div class="ShowTicket">

<table>
	<tr>
		<td>ticket id</td>
		<td>${requestScope.tickets.ticket_id}</td>
	</tr>
	<tr>
		<td>ticket name&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>${requestScope.tickets.name}</td>
	</tr>
	
	
	<tr></tr>
	
	<tr>
		<td>ticket price</td>
		<td>${requestScope.tickets.ticket_price}</td>
	</tr>
	</table>

	<table>
	<tr>
		<td>address&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>${requestScope.tickets.address}</td>
	</tr>
	</table>
<br><br>

</div>

<div class="description">
	<br><br>
<table>
	
	<tr>
		<td>description&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td width="400px">${requestScope.tickets.description}</td>
	</tr>

</table>
<br><br>
<a href="showTDetail.do?flag=buy&&ticket_id=${requestScope.tickets.ticket_id}">Buy Tickets</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="Index.jsp">Back to the main page</a>
</div>
</body>
</html>