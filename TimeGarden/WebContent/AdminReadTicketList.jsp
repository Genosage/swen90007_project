<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="<%=basePath%>">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/ticketManagement.css">
</head>
<body>
<div class="background">
	<img src="images/02.jpg" width="1200px" height="800px"/>
</div>
<div class="background2">
	<img src="images/02.jpg" width="1200px" height="800px"/>
</div>
<div class="background3">
	<img src="images/02.jpg" width="1200px" height="800px"/>
</div>
<h1>Time Garden Administration Page</h1>

<div class="operation_list">
	<table>
		<tr>
			<th><a href="AdminAddTickets.jsp">Add Tickets</a><br><br></th>
		</tr>
		
		<tr>
			<th><a href="showTicket.do?flag=adminReadTicket">Modify Tickets</a><br><br></th>
		</tr>
		
		<tr>
			<th><a href="showTicket.do?flag=adminReadTicket">Delete Tickets</a><br><br></th>
		</tr>
		
		<tr>
			<th><a href="showTicket.do?flag=adminReadTicket">Read Tickets</a><br><br></th>
		</tr>
		
		<tr>
			<th><a href="AdminTicketManagement.jsp">Back</a></th>
		</tr>
	</table>
 
</div>

<div class="ticket_form">

<table>
<c:forEach var="tickets" items="${requestScope.tickets}">	
	<tr>
		<td><img src="images/${tickets.image }" width="200px" height="100px"/></td>
		<td>
			Ticket ID: &nbsp;&nbsp;&nbsp;${tickets.ticket_id}<br><br>
			Ticket Name:&nbsp;&nbsp;${tickets.name }<br><br>
			<a href="showTDetail.do?flag=adminReadTicketDetail&ticket_id=${tickets.ticket_id }">Show Details</a>
		</td>
	</tr>
</c:forEach>

</table>

</div>
</body>
</html>