<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="<%=basePath%>">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/AdminPage.css">
</head>
<body>
<div class="background">
	<img src="images/02.jpg" width="1200px" height="800px"/>
</div>
<h1>Time Garden Administration Page</h1>
<div class="ShowTicket1">
<h3>Administrator Personal Order List Page</h3>
</div>
<div class="ShowTicket">
<c:forEach var="orders" items="${requestScope.showPersonalOrders}">	

	<table>
	<tr>
		<td>order id:&nbsp;&nbsp;&nbsp;</td>
		<td>${orders.order_id }&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>order price:&nbsp;&nbsp;&nbsp;</td>
		<td>${orders.order_price }&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>user:&nbsp;&nbsp;&nbsp;</td>
		<td>${orders.email }</td>
		<td>
			<a href="showOrderDetail.do?flag=adminRead&order_id=${orders.order_id }&ticket_id=${orders.ticket_id}">see detail</a><br>
		</td>
	</tr>
</table>

</c:forEach><br><br>
<a href="AdminIndex.jsp">Back to the main page</a>
</div>
</body>
</html>