<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="<%=basePath%>">

<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/ticketManagement.css">
</head>
<body>
<div class="background">
	<img src="images/02.jpg" width="1200px" height="800px"/>
</div>
<h1>Time Garden Administration Page</h1>

<div class="operation_list">
	<table>
		<tr>
			<td><a href="readAllUser.do">Read User Information</a><br><br></td>
		</tr>
		<tr>
			<td><a href="identifyPowerUser.do">Power User Management</a><br><br></td>
		</tr>
		
		<tr>
			<td><a href="addTag.do">User Interest Management</a><br><br></td>
		</tr>
		
		<tr>
			<td><a href="AdminManageUsers.jsp">Back</a></td>
		</tr>
	</table>
 
</div>
<div class="ticket_form2">
	<table>
		<tr>
			<td>
				New Power User<br>
				payment larger or equals to 1000 AUD dollars
			</td>
		</tr>
		
		<c:forEach var="UserEmail" items="${requestScope.PowerUserEmail}">	
			<tr>
				<td>${UserEmail}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td><a href="addPower.do?email=${UserEmail}">Add Power</a></td>
				<td><a href="showAllOrder.do?flag=showPersonalOrders&email=${UserEmail}">See order details</a></td>
			</tr>
		</c:forEach>
			
		
	</table><br>
	-----------------------------------------------------------------------------------
	<table>
		<tr>
			<td>
				Tagged Power User<br>
				payment larger or equals to 1000 AUD dollars<br> and have already have power
			</td>
		</tr>
		
		<c:forEach var="UserEmail" items="${requestScope.taggedPowerUserEmail}">	
			<tr>
				<td>${UserEmail}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td><a href="showAllOrder.do?flag=showPersonalOrders&email=${UserEmail}">See order details</a></td>
				<td></td>
			</tr>
		</c:forEach>
			
		
	</table><br>
	-----------------------------------------------------------------------------------
	<table>
		<tr>
			<td>
				Not Power User<br>
				payment smaller to 1000 AUD dollars
			</td>
		</tr>
		
		<c:forEach var="UserEmail" items="${requestScope.UnPowerUserEmail}">
			<tr>	
				<td>${UserEmail}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td><a href="showAllOrder.do?flag=showPersonalOrders&email=${UserEmail}">See order details</a></td>
			</tr>
		</c:forEach>
</table>
</div>
</body>
</html>