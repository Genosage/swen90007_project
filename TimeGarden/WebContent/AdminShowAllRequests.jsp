<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="<%=basePath%>">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/ticketManagement.css">
</head>

<body>
<div class="background">
	<img src="images/02.jpg" width="1200px" height="800px"/>
</div>
<h1>Time Garden Administration Page</h1>
<div class="ShowTicket1">
Administrator Request List Page
</div>

<div class="ShowAllRequest">

<c:forEach var="requests" items="${requestScope.requests}">	
<table>
	<tr>
		<td>requests order id:&nbsp;&nbsp;&nbsp;</td>
		<td>${requests.order_id }&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>request type:&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>${requests.request_type}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>request status:&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>${requests.request_status}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>
		<a href="checkRequestDetail.do?request_id=${requests.request_id }&order_id=${requests.order_id }">check details and respond</a><br>
		</td>
	</tr>
</table>
	
</c:forEach>
<br><br>
<a href="AdminIndex.jsp">Back to the main page</a>
</div>
</body>
</html>