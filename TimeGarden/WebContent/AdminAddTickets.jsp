<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<%@ page import="DBOperation.*"%>   

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="<%=basePath%>">
<title>Insert title here</title>
<script type="text/javascript">

	function isNull(){
		var ticket_type = document.forms[0].ticket_type.value;
		var ticket_price = parseInt(document.forms[0].ticket_price.value);
		var name = document.forms[0].name.value;
		var address = document.forms[0].address.value;
		var image = document.forms[0].image.value;
		var description = document.forms[0].description.value;
		
		if(ticket_type == ""){
			alert("please input a number");
			document.forms[0].ticket_type.focus();
			return false;
		}
		
		if(ticket_price=""){
			alert("Please input your price");
			document.forms[0].ticket_price.focus();
			return false;
		}
		
		if(address == ""){
			alert("please input the ticket address")
			document.forms[0].address.focus();
			return false;
		}
		
		if(description == ""){
			alert("please input the ticket description")
			document.forms[0].description.focus();
			return false;
		}
		document.forms[0].submit();
		return true;
    }
</script>
<link rel="stylesheet" type="text/css" href="css/ticketManagement.css">
</head>
<body>
<div class="background">
	<img src="images/02.jpg" width="1200px" height="800px"/>
</div>
<h1>Time Garden Administration Page</h1>
<div class="operation_list">
	<table>
		<tr>
			<th><a href="AdminAddTickets.jsp">Add Tickets</a><br><br></th>
		</tr>
		
		<tr>
			<th><a href="showTicket.do?flag=adminReadTicket">Modify Tickets</a><br><br></th>
		</tr>
		
		<tr>
			<th><a href="showTicket.do?flag=adminReadTicket">Delete Tickets</a><br><br></th>
		</tr>
		
		<tr>
			<th><a href="showTicket.do?flag=adminReadTicket">Read Tickets</a><br><br></th>
		</tr>
		
		<tr>
			<th><a href="AdminTicketManagement.jsp">Back</a></th>
		</tr>
	</table>
 
 
</div>

<div class="title">
please complete the following form.
</div>

<div class="ticket_form">

<form action="addTickets.do" method="post" name="AddTicketsForm">
<table>
	<tr>
		<td>ticket_id</td>
		<td><input type="text" value="" id="ticket_id" name="ticket_id"/>
		</td>
		<td><font color="red">*</font></td>
	</tr>
	<tr>
		<td>ticket_type</td>
		<td>
			<select name="ticket_type">
				<option value="adult" selected> adult
				<option value="child">child
				<option value="concession">concession
				<option value="senior"> senior
			</select>
			</td>
			<td><font color="red">*</font></td>
	</tr>
	
	<tr>
		<td>ticket price&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td><input type="text" value="" id="ticket_price" name="ticket_price"/>
		</td>
		<td><font color="red">*</font></td>
	</tr>
	
	<tr>
		<td>ticket name</td>
		<td>
			<select id="name" name="name">
				<option value="Hour Park" selected> Hour Park
				<option value="Minute Park">Minute Park
				<option value="Second Park">Second Park
			</select>
			</td>
			<td><font color="red">*</font></td>
	</tr>
	
	<tr>
		<td>address</td>
		<td><input type="text" value="address" id="address" name="address"/></td>
		<td><font color="red">*</font></td>
	</tr>
	
	<tr>
		<td>image</td>
		<td><input type="text" value="image" id="name" name="image"/></td>
	</tr>
	
	<tr>
		<td>description</td>
		<td><input type="text" value="description" id="description"name="description"/></td>
		<td><font color="red">*</font></td>
	</tr>
</table>
<br>
<input type="button"  value="submit" onclick="isNull()"/>
	</form>
</div>
</body>

</html>