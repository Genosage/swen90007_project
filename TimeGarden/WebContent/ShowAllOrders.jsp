<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="<%=basePath%>">

<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body style="background:url(./images/01.jpg) no-repeat;">
<h1>Time Garden</h1>
<div class="ShowTicket1">
<h3>Order List</h3>
</div>
<div class="ShowTicket22">

<c:forEach var="orders" items="${requestScope.personalAllOrders}">	
<table>
	<tr>
		<td>order id:&nbsp;&nbsp;&nbsp;</td>
		<td>${orders.order_id }&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>order price:&nbsp;&nbsp;&nbsp;</td>
		<td>${orders.order_price }&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>
			<a href="showOrderDetail.do?order_id=${orders.order_id }">I want see detail</a><br>
		</td>
	</tr>
</table>

	
</c:forEach>

	<br><br>
<a href="Index.jsp">Back to the main page</a>
</div>
</body>
</html>