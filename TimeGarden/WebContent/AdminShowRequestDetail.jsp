<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/ticketManagement.css">
</head>

<body>
<div class="background">
	<img src="images/02.jpg" width="1200px" height="800px"/>
</div>
<h1>Time Garden Administration Page</h1>
<div class="ShowTicket1">
Administrator Request Detail Page
</div>

<div class="ShowRequestDetail">
<table>
	<tr>
		<td>request id</td>
		<td>${requestScope.request_id}</td>
	</tr>
	<tr>
		<td>order id</td>
		<td>${requestScope.request.order_id}</td>
	</tr>
	<tr>
		<td>request type</td>
		<td>${requestScope.request.request_type}</td>
	</tr>
	<tr>
		<td>new ticket name</td>
		<td>${requestScope.request.newticket_name}</td>
	</tr>
	<tr>
		<td>new ticket type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>${requestScope.request.newticket_type}</td>
	</tr>
	
	<tr>
		<td>new ticket number</td>
		<td>${requestScope.request.newticket_number}</td>
	</tr>
	<tr>
		<td>order status</td>
		<td>${requestScope.request.request_status}</td>
	</tr>

</table>
<br><br>
<a href="deleteOrder.do?order_id=${requestScope.request.order_id}& request_id=${requestScope.request_id}">delete the corresponding order</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="showOrderDetail.do?flag=modify&order_id=${requestScope.request.order_id}&ticket_id=${requestScope.ticket_id}">modify the corresponding order</a>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="showOrderDetail.do?flag=modify&order_id=${requestScope.request.order_id}&ticket_id=${requestScope.ticket_id} ">Refuse this request</a><br><br>
<br><br><a href="AdminIndex.jsp">Back to the main page</a>
</div>
</body>
</html>