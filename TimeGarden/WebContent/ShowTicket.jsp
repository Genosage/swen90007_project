<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="<%=basePath%>">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body style="background:url(./images/01.jpg) no-repeat;">
<h1>Time Garden</h1>
<div class="ShowTicket1">
<h3>All Tickets</h3>
</div>
<div class="ShowTicket22">
<table>
<c:forEach var="tickets" items="${requestScope.tickets}">	
	<tr>
		<td><img src="images/${tickets.image }" width="200px" height="100px"/></td>
		<td>
			Ticket ID: &nbsp;&nbsp;&nbsp;${tickets.ticket_id}<br><br>
			Ticket Name:&nbsp;&nbsp;${tickets.name }<br><br>
		<a href="showTDetail.do?ticket_id=${tickets.ticket_id }">Show Details</a><br><br>
		</td>
	</tr>
</c:forEach>

</table>
</div>
</body>
</html>