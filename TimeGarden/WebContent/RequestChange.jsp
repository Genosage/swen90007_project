<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="<%=basePath%>">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body style="background:url(./images/01.jpg) no-repeat;">
<h1>Time Garden</h1>
<div class="ShowTicket1">
<h3>Change Order Request</h3>
</div>
<div class="ShowTicket22">

<form action="createRequest.do" method="post" name="CreateRequestForm">
		your order id is: &nbsp;<input type="text" name="order_id" value="${requestScope.orders.order_id}" readonly/>

	<h3>what do you want to do?</h3>
		<input type="radio" name="request_type" value="Modify" checked/> Modify&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="radio" name="request_type" value="Delete"/> Delete<br>

--------------------------------------------------------------------------------------------------------
	<h3>New Ticket choice</h3>

		Please choose new ticket name
			<select name="newticket_name">
				<c:forEach var="unique" items="${requestScope.unique_ticket_name}">	
					<option value="${unique.name }" > 
							${unique.name }  
					</c:forEach>
			</select>
		<br><br>

		Please choose new ticket type
			<select name="newticket_type">
				<c:forEach var="unique" items="${requestScope.unique_ticket_type}">	
					<option value="${unique.ticket_type }" > 
							${unique.ticket_type }  
				</c:forEach>
			</select>
<br><br>

		Please input new ticket amount
		<input type="text" name="newticket_number" value="1"/><br><br>

------------------------------------------------------------------------------------------------------------
<br><br>
		Status of this request
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="text" name="request_status" value="0" disabled/>
		<input type="submit" value="submit"/>
</form>
</div>
</body>
</html>

<!-- 
request_id
order_id
request_type
newticket_name
newticket_type
newticket_number
request_status
 -->