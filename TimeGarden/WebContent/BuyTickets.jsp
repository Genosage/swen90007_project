<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<%@ page import="DBOperation.*"%>   

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="<%=basePath%>">
<title>Insert title here</title>
<script type="text/javascript">

function sum(){
	var power = parseInt(document.getElementById("power").value);
	var price = parseInt(document.getElementById("price").value);
	var mkrum = parseInt(document.getElementById("mkrum").value);
	if(power == 1){
		var o_price =  mkrum*price*0.5;
	}else{
		var o_price = mkrum*price;
	}
	
	document.forms[0].elements["order_price"].value = o_price;
}

	function isNull(){
		var ticket_number=parseInt(document.forms[0].ticket_number.value);
		var email = parseInt(document.forms[0].email.value);
		var ticket_price = parseInt(document.forms[0].ticket_price.value);
		var order_price = parseInt(document.forms[0].order_price.value);
		
		if(ticket_number==""){
			alert("please input a number");
			document.forms[0].ticket_number.focus();
			return false;
		}
		
		if(email=""){
			alert("Please input your email");
			document.forms[0].email.focus();
			return false;
		}
		if(ticket_price==""){
			alert("cannot be 0");
			document.forms[0].ticket_price.focus();
		}
		if(order_price==""){
			alert("cannot be 0")
			document.forms[0].order_price.focus();
			return false;
		}
		document.forms[0].submit();
		return true;
    }
</script>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body style="background:url(./images/01.jpg) no-repeat;">
<h1>Time Garden</h1>

<div class="buyTicket1">
<h3>please choose what you want.</h3>
</div>

<div class="ShowTicket22">
<form action="buyTickets.do" method="post" >
<table>
	<tr>
		<td>ticket_name</td>
		<td><input type="text" value="${requestScope.tickets.ticket_id}" name="ticket_id" readonly/></td>
	</tr>
	
	<tr>
		<td>per ticket price</td>
		<td><input type="text" value="${requestScope.tickets.ticket_price}" id="price" name="ticket_price" readonly/></td>
	</tr>
	
	<tr>
		<td>purchase number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td><input type="text" id="mkrum" name="ticket_number" value="1"/></td>
	</tr>
	
	<tr>
		<td>total price</td>
		<td><input type="text" id="total" name="order_price" onclick="sum()" value="plase click here"/></td>
	</tr>
	
	<tr>
		<td>your email</td>
		<td><input type="text" name="email"/></td>
	</tr>
	
	<tr>
		<td>your Power</td>
		<td><input type="text" value="${requestScope.power }" name="power" id="power" readonly/></td>
		<td>* 1 means you have 50% discount for this ticket <br>  0 means you need to pay the full price<td>
	</tr>
</table>
<br>
<input type="button"  value="submit" onclick="isNull('${requestScope.tickets.ticket_price}')"/>
	</form>
</div>
</body>

</html>