<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="<%=basePath%>">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/ticketManagement.css">
</head>

<body>
<div class="background">
	<img src="images/02.jpg" width="1200px" height="800px"/>
</div>
<h1>Time Garden Administration Page</h1>
<div class="ShowTicket1">
Administrator Order Detail Page
</div>
<div class="ShowRequestDetail">

Original Order
	<form action="modifyOrder.do" method="post" name="modifyActivityForm">
	<table>
		<tr>
			<td>order_id</td>
			<td><input type="text" name="order_id"  readonly value="${requestScope.orders.order_id }"/></td>
		</tr>
		
		<tr>
			<td>order_price&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td><input type="text" name="order_price"  value="${requestScope.orders.order_price }"/></td>
		</tr>
		
		<tr>
			<td>email</td>
			<td><input type="text" name="email"  value="${requestScope.orders.email }"/></td>
		</tr>
	</table>
 
  	-----------------------------------------------------<br><br>
 	Original ticket information
  	<table>
  		<tr>
  			<td>ticket id</td>
  			<td>${requestScope.orders.ticket_id }</td>
  		</tr>
  		
  		<tr>
  			<td>ticket name</td>
  			<td>${requestScope.ticket.name }</td>
  		</tr>
  		
  		<tr>
  			<td>ticket type</td>
  			<td>${requestScope.ticket.ticket_type }</td>
  		</tr>
  		
  		<tr>
  			<td>ticket number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  			<td>${requestScope.orders.ticket_number }</td>
  		</tr>
  	</table>
  	-----------------------------------------------------------------------<br>
  	Select New ticket
  	<table>
  		<tr>
  			<td>Please choose new ticket name</td>
  			<td>
  				<select name="newticket_name">
					<c:forEach var="unique" items="${requestScope.unique_ticket_name}">	
						<option value="${unique.name }" > 
							${unique.name }  
					</c:forEach>
				</select>
  			</td>
  		</tr>
  		
  		<tr>
  			<td>Please choose new ticket type</td>
  			<td>
  				<select name="newticket_type">
					<c:forEach var="unique" items="${requestScope.unique_ticket_type}">	
						<option value="${unique.ticket_type }" > 
							${unique.ticket_type }  
					</c:forEach>
				</select>
  			</td>
  		</tr>
  		
  		<tr>
  			<td>Please input new ticket amount&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
  			<td><input type="text" name="newticket_number" value="1"/></td>
  		</tr>
  	</table>
 
-----------------------------------------------------------------------------<br><br>
	order_status&nbsp;&nbsp;<input type="text" name="order_status" value="${requestScope.orders.order_status }"/><br>
  	<br><br>
 
  	<input type="submit" value="submit"/> 
  	</form>
  	</div>
</body>
</html>