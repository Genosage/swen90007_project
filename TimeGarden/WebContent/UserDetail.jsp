<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="<%=basePath%>">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body style="background:url(./images/01.jpg) no-repeat;">
<h1>Time Garden</h1>
<div class="ShowTicket1">
<h3>Personal Information</h3>
</div>
<div class="ShowTicket22">
<table>
	<tr>
		<td>User id</td>
		<td>${requestScope.userDetail.user_id}</td>
	</tr>
	<tr>
		<td>email</td>
		<td>${requestScope.userDetail.email}</td>
	</tr>
	<tr>
		<td>password&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>${requestScope.userDetail.password}</td>
	</tr>
	<tr>
		<td>power</td>
		<td>${requestScope.userDetail.power}</td>
	</tr>
	<tr>
		<td>hour ticket number</td>
		<td>${requestScope.userDetail.hourticket_number}</td>
	</tr>
	<tr>
		<td>minute ticket number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>${requestScope.userDetail.minuteticket_number}</td>
	</tr>
	
	<tr>
		<td>second ticket number</td>
		<td>${requestScope.userDetail.secondticket_number}</td>
	</tr>
	
	<tr>
		<td>interest</td>
		<td>${requestScope.userDetail.tag}</td>
	</tr>
	
</table>

<a href="Index.jsp">Back to the main page</a>
</div>
</body>
</html>