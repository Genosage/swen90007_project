<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="<%=basePath%>">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/ticketManagement.css">
</head>
<body>
<div class="background">
	<img src="images/02.jpg" width="1200px" height="800px"/>
</div>
<h1>Time Garden Administration Page</h1>
<div class="operation_list">
	<table>
		<tr>
			<th><a href="AdminAddTickets.jsp">Add Tickets</a><br><br></th>
		</tr>
		
		<tr>
			<th><a href="showTicket.do?flag=adminReadTicket">Modify Tickets</a><br><br></th>
		</tr>
		
		<tr>
			<th><a href="showTicket.do?flag=adminReadTicket">Delete Tickets</a><br><br></th>
		</tr>
		
		<tr>
			<th><a href="showTicket.do?flag=adminReadTicket">Read Tickets</a><br><br></th>
		</tr>
		
		<tr>
			<th><a href="AdminTicketManagement.jsp">Back</a></th>
		</tr>
	</table>
 
</div>

<div class="title">Ticket Information</div>
<div class="ticket_form">

	<form action="modifyTicket.do" method="post" name="ModifyTicketForm">
	<table>
		<tr>
			<td>ticket id</td>
			<td>
				<input type="text" name="ticket_id"  value="${requestScope.tickets.ticket_id}" readonly>
			</td>
		</tr>
		
		<tr>
			<td>the original ticket name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>${requestScope.tickets.name}</td>
		</tr>
		
		<tr>
			<td>the new ticket name</td>
			<td>
				<select name="name">
					<c:forEach var="unique" items="${requestScope.unique_name}">	
						<option value="${unique.name }" > 
							${unique.name }  
					</c:forEach>
				</select>
			</td>
		</tr>
		
		<tr>
			<td>the original ticket type</td>
			<td>${requestScope.tickets.ticket_type}</td>
		</tr>
	
		<tr>
			<td>the new ticket type</td>
			<td>
				<select name="ticket_type">
					<c:forEach var="unique" items="${requestScope.unique_type}">	
						<option value="${unique.ticket_type }" > 
							${unique.ticket_type }  
					</c:forEach>
				</select>
			</td>
		</tr>
		
		<tr>
			<td>ticket price&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td><input type="text" name="ticket_price"  value="${requestScope.tickets.ticket_price}"/></td>
		</tr>
		
		<tr>
			<td>address</td>
			<td><input type="text" name="address"  value="${requestScope.tickets.address }"/></td>
		</tr>
		
		<tr>
			<td>image</td>
			<td><input type="text" name="image"  value="${requestScope.tickets.image }"/></td>
		</tr>
		<tr>
			<td>description</td>
			<td><input type="text" name="description"  value="${requestScope.tickets.description }"/></td>
		</tr>
	</table>
	<br><br>
  	<input type="submit" value="submit"/> 
  	</form>
  	</div>
</body>
</html>