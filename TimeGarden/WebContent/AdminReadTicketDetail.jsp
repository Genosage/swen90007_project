<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="<%=basePath%>">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="css/ticketManagement.css">
</head>
<body>
<div class="background">
	<img src="images/02.jpg" width="1200px" height="800px"/>
</div>
<h1>Time Garden Administration Page</h1>
<div class="operation_list">
	<table>
		<tr>
			<th><a href="AdminAddTickets.jsp">Add Tickets</a><br><br></th>
		</tr>
		
		<tr>
			<th><a href="showTicket.do?flag=adminReadTicket">Modify Tickets</a><br><br></th>
		</tr>
		
		<tr>
			<th><a href="showTicket.do?flag=adminReadTicket">Delete Tickets</a><br><br></th>
		</tr>
		
		<tr>
			<th><a href="showTicket.do?flag=adminReadTicket">Read Tickets</a><br><br></th>
		</tr>
		
		<tr>
			<th><a href="AdminTicketManagement.jsp">Back</a></th>
		</tr>
	</table>
 
</div>
<div class="ticket_pic">
<img src="images/${requestScope.tickets.image}" width="300px" height="200px"/>
<br><br>
<hr color="orange" width="400px" />

</div>

<div class="ticket_form1">
<table>
	<tr>
		<td>ticket id</td>
		<td>${requestScope.tickets.ticket_id}</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>ticket name&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>${requestScope.tickets.name}</td>
	</tr>
	
	
	<tr></tr>
	
	<tr>
		<td>ticket price</td>
		<td>${requestScope.tickets.ticket_price}</td>
		<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td>address</td>
		<td>${requestScope.tickets.address}</td>
	</tr>
	</table>
	<table>
	
	<tr>
		<td>description</td>
		<td width="400px">${requestScope.tickets.description}</td>
	</tr>

</table>
<br><br>
<a href="showTDetail.do?flag=adminModifyTicket&ticket_id=${requestScope.tickets.ticket_id}">Modify this Ticket</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="deleteTicket.do?ticket_id=${requestScope.tickets.ticket_id}">Delete this Ticket</a>
</div>

</body>
</html>