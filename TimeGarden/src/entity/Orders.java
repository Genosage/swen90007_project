package entity;


public class Orders {
	private Integer order_id;
	private Integer ticket_id;
	private Integer ticket_number;
	private Integer order_price;
	private String email;
	private Integer order_status;
	
	public Integer getOrder_id() {
		return order_id;
	}
	public void setOrder_id(Integer order_id) {
		this.order_id = order_id;
	}
	public Integer getTicket_id() {
		return ticket_id;
	}
	public void setTicket_id(Integer ticket_id) {
		this.ticket_id = ticket_id;
	}
	public Integer getTicket_number() {
		return ticket_number;
	}
	public void setTicket_number(Integer ticket_number) {
		this.ticket_number = ticket_number;
	}
	public Integer getOrder_price() {
		return order_price;
	}
	public void setOrder_price(Integer order_price) {
		this.order_price = order_price;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getOrder_status() {
		return order_status;
	}
	public void setOrder_status(Integer order_status) {
		this.order_status = order_status;
	}
	
	
}
