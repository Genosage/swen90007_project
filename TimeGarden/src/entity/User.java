package entity;

public class User {
	private int user_id;
	private String email;
	private int password;
	private int hourticket_number;
	private int minuteticket_number;
	private int secondticket_number;
	private String tag;
	private int power;
	
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getPassword() {
		return password;
	}
	public void setPassword(int password) {
		this.password = password;
	}
	public int getHourticket_number() {
		return hourticket_number;
	}
	public void setHourticket_number(int hourticket_number) {
		this.hourticket_number = hourticket_number;
	}
	public int getMinuteticket_number() {
		return minuteticket_number;
	}
	public void setMinuteticket_number(int minuteticket_number) {
		this.minuteticket_number = minuteticket_number;
	}
	public int getSecondticket_number() {
		return secondticket_number;
	}
	public void setSecondticket_number(int secondticket_number) {
		this.secondticket_number = secondticket_number;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	
}
