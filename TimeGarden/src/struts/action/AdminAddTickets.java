package struts.action;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.messageOperation;
import DBOperation.ticketOperation;
import DBOperation.userOperation;
import entity.User;
import struts.form.AddTicketsForm;

public class AdminAddTickets extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		// TODO Auto-generated method stub
		AddTicketsForm addTicketsForm = (AddTicketsForm) form;
		String string = addTicketsForm.getName();
		
		ticketOperation tO = new ticketOperation();
		tO.addTickets(addTicketsForm);
		
		//get all users' email
		userOperation uOperation = new userOperation();
		ArrayList<User> au = uOperation.getAllUsersTag();
		
		messageOperation mOperation = new messageOperation();
		Iterator<User> user_id = au.iterator();
		
		while(user_id.hasNext()){
			User user = user_id.next();
			if (user.getTag()!= null && (user.getTag()).equals(string)) {
				mOperation.addMessage(addTicketsForm.getTicket_id(),user.getEmail());
			}
		}
			return mapping.findForward("success");
	}
}
