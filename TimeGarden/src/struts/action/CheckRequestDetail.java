package struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.requestOperation;
import entity.Requests;

public class CheckRequestDetail extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		
		String id=request.getParameter("request_id");
		int order_id = Integer.parseInt(request.getParameter("order_id"));
		System.out.println(request.getParameter("order_id"));
		System.out.println(order_id);
		String flag=request.getParameter("flag");
		requestOperation ro=new requestOperation();
		
		//check the ticket_id based on the order_id
		int ticket_id = ro.getTicketId(order_id);
		//System.out.println(ticket_id);
		
		request.setAttribute("ticket_id", ticket_id);
		
		Requests t=ro.getARequest(id);
		
		request.setAttribute("request", t);
		request.setAttribute("request_id", id);
		if("customerRead".equals(flag))
			return mapping.findForward("customerRead");
		else
			return mapping.findForward("success");
	}
}
