package struts.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.userOperation;
import entity.User;

public class AdminReadAllUsers extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		userOperation uo = new userOperation();
		ArrayList<User> arrayList = uo.getAllUsers();
		request.setAttribute("getAllUsers", arrayList);
		return mapping.findForward("readAllUsers");
	}
}
