package struts.action;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.orderOperation;
import DBOperation.userOperation;
import entity.Orders;
import entity.User;
import struts.form.LoginForm;

public class ShowAllOrder extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		String flag=request.getParameter("flag");
		String email = request.getParameter("email");
		HttpSession session = request.getSession(true);
		LoginForm loginForm = (LoginForm)session.getAttribute("user");
	
		
		orderOperation gs=new orderOperation();
		ArrayList<Orders> al=gs.getAllOrders();
		request.setAttribute("orders", al);
		
		
		
		if("adminRead".equals(flag)){
			return mapping.findForward("adminRead");
		}else if ("adminReadOnly".equals(flag)) {
			return mapping.findForward("adminReadOnly");
		}else if ("addTag".equals(flag)) {
			
			//get all user emails
			userOperation uOperation = new userOperation();
			ArrayList<User> au = uOperation.getAllEmails();
			
			//search each email's orders
			Iterator<User> iterator = au.iterator();
			orderOperation operation = new orderOperation();
			ArrayList<Orders> allPersonalOrders = new ArrayList<>();
			//encapsulate all personal orders by email
			while(iterator.hasNext()){
				ArrayList<Orders> ao = operation.getAllPersonalOrders(iterator.next().getEmail());
				allPersonalOrders.addAll(ao);
			}
			
			//set request attribute
			request.setAttribute("AllOrders", allPersonalOrders);
			return mapping.findForward("addTag");
			
			}else if ("showPersonalOrders".equals(flag)) {
				orderOperation operation = new orderOperation();
				ArrayList<Orders> ao = operation.getAllPersonalOrders(email);
				request.setAttribute("showPersonalOrders", ao);
				return mapping.findForward("showPersonalOrders");
			}else{
				
			ArrayList<Orders> allPersonalOrders = gs.getAllPersonalOrders(loginForm.getEmail());
			request.setAttribute("personalAllOrders", allPersonalOrders);
			return mapping.findForward("success");
			
		}
			
	}
}
