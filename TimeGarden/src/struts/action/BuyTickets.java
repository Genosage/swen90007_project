package struts.action;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


import DBOperation.orderOperation;
import DBOperation.ticketOperation;
import DBOperation.userOperation;
import entity.Tickets;
import struts.form.BuyTicketForm;




public class BuyTickets extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		// TODO Auto-generated method stub
		
		BuyTicketForm buyTicketForm = (BuyTicketForm) form;
		
		orderOperation mb=new orderOperation();
		userOperation uOperation = new userOperation();
		
		ticketOperation tOperation = new ticketOperation();
		Tickets ticket= tOperation.getAticket(String.valueOf(buyTicketForm.getTicket_id()));
		if ("Hour Park".equals(ticket.getName())) {
			uOperation.updateHourTicketNo(buyTicketForm.getTicket_number(),buyTicketForm.getEmail());
		}else if ("Minute Park".equals(ticket.getName())) {
			uOperation.updateMinuteTicketNo(buyTicketForm.getTicket_number(),buyTicketForm.getEmail());
		}else if ("Second Park".equals(ticket.getName())) {
			uOperation.updateSecondTicketNo(buyTicketForm.getTicket_number(),buyTicketForm.getEmail());
		}
		if(mb.addOrder(buyTicketForm))
			return mapping.findForward("success");
		else
			return mapping.findForward("fail");
	
	}
}
