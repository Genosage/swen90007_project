package struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


import DBOperation.LoginOperation;
import struts.form.RegisterForm;

public class Register extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		// TODO Auto-generated method stub
		RegisterForm registerFrom = (RegisterForm) form;
		//System.out.println(registerFrom.getEmail());
		LoginOperation loginOperation = new LoginOperation();
		if(loginOperation.addUser(registerFrom))
			return mapping.findForward("success");
		else
			return mapping.findForward("fail");
	
	}
}
