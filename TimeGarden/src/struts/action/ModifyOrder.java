package struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.orderOperation;
import struts.form.ModifyOrderForm;

public class ModifyOrder extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		ModifyOrderForm modifyOrderForm = (ModifyOrderForm) form;
		
		String newTicket_name = modifyOrderForm.getNewticket_name();
		String newTicket_type = modifyOrderForm.getNewticket_type();

		orderOperation orderOperation = new orderOperation();
		
		int ticket_id = orderOperation.getTicketId(newTicket_name, newTicket_type);
		
		
		int newticket_price = orderOperation.getTicketPrice(ticket_id);
		
		int newticket_number = modifyOrderForm.getNewticket_number();
		int order_price = newticket_price*newticket_number;
		
		if(orderOperation.modifyOrder(modifyOrderForm,ticket_id,order_price))
			return mapping.findForward("success");
		else
			return mapping.findForward("fail");
	}
}
