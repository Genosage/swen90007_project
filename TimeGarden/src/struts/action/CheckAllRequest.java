package struts.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.requestOperation;
import entity.Requests;
import struts.form.LoginForm;

public class CheckAllRequest extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		String flag=request.getParameter("flag");
		
		//get user email
		HttpSession session = request.getSession(true);
		LoginForm loginForm = (LoginForm)session.getAttribute("user");
		
		//get all request without specific user email
		requestOperation ro=new requestOperation();
		ArrayList<Requests> al=ro.getAllRequests();
		request.setAttribute("requests", al);
		
		//get specific user requests
		
	
		
		if ("customerRead".equals(flag)) 
		{
			ArrayList<Requests> aOrders = ro.getAllPersonalRequests(loginForm.getEmail());
			request.setAttribute("personalRequests", aOrders);
			return mapping.findForward("customerRead");
		}
			
		else
			return mapping.findForward("success");
	}
}
