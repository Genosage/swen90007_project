package struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.userOperation;
import entity.User;
import struts.form.LoginForm;

public class ReadOwnData extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(true);
		LoginForm loginForm = (LoginForm)session.getAttribute("user");
		
		String id = request.getParameter("user_id");
		String flag = request.getParameter("flag");
		
		userOperation uOperation = new userOperation();
		
		if ("AdminRead".equals(flag)) {
			User u1 = uOperation.getUserDetail(id);
			request.setAttribute("userDetail", u1);
			return mapping.findForward("AdminreadAllUsers");
		}else{
			int user_id = uOperation.getUserID(loginForm.getEmail());
			String id2 = String.valueOf(user_id);
			User u = uOperation.getUserDetail(id2);
			request.setAttribute("userDetail", u);
		}
		return mapping.findForward("readOwnData");
	}
}
