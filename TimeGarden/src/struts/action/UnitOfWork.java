package struts.action;

import java.util.ArrayList;

import com.sun.xml.internal.ws.policy.sourcemodel.AssertionData;

import DBOperation.orderOperation;


public class UnitOfWork {
	private static ThreadLocal current = new ThreadLocal();
	private ArrayList<Object> newObjects = new ArrayList<Object>(); 
	private ArrayList<Object> dirtyObjects = new ArrayList<Object>(); 
	private ArrayList<Object> deletedObjects = new ArrayList<Object>();
	public static void newCurrent() { setCurrent(new UnitOfWork());
	}
	public static void setCurrent(UnitOfWork uow) { 
		current.set(uow);
	}
	public static UnitOfWork getCurrent() { return (UnitOfWork) current.get();
	}

	public void registerNew(Object obj) 
	{assert(!obj.equals(null)); 
	assert(!dirtyObjects.contains(obj)); 
	assert(!deletedObjects.contains(obj)); 
	assert(!newObjects.contains(obj));
	}
	public void registerDirty(Object obj) {
		assert(!obj.equals(null));
	assert(!deletedObjects.contains(obj)); 
	if (!dirtyObjects.contains(obj) && !newObjects.contains(obj)) 
	{
	    dirtyObjects.add(obj);
	  }
	}
	public void registerDeleted(Object obj) { assert(!obj.equals(null));
	if (newObjects.remove(obj)) return; dirtyObjects.remove(obj);
	if (!deletedObjects.contains(obj)) { deletedObjects.add(obj);
	} }
	public void registerClean(Object obj) { assert(!obj.equals(null));
	}
	
	public void commit() {
	for (Object obj : newObjects) {
		orderOperation.getOneCon();
	   // DataMapper.getMapper(obj.getClass()).insert(obj);
	  }
	for (Object obj : dirtyObjects) { 
		orderOperation.getOneCon();
		//DataMapper.getMapper(obj.getClass()).update(obj);
	}
	for (Object obj : deletedObjects) {
		orderOperation.getOneCon();
	   // DataMapper.getMapper(obj.getClass()).delete(obj);
	  }
	}
}
