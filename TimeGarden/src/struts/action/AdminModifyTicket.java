package struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.ticketOperation;
import struts.form.ModifyTicketForm;

public class AdminModifyTicket extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
	
		ModifyTicketForm modifyOrderForm = (ModifyTicketForm) form;
		
		ticketOperation tOperation = new ticketOperation();
		
		if(tOperation.modifyTicket(modifyOrderForm))
			return mapping.findForward("success");
		else
			return mapping.findForward("fail");
	}
}
