package struts.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.orderOperation;
import DBOperation.ticketOperation;
import entity.Orders;
import entity.Tickets;
import struts.form.LoginForm;

public class ShowOrderDetail extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(true);
		LoginForm loginForm = (LoginForm)session.getAttribute("user");
		
		String id=request.getParameter("order_id");
		String flag=request.getParameter("flag");
		String ticket_id = request.getParameter("ticket_id");
		
		orderOperation gb=new orderOperation();
		Orders o=gb.getAnOrder(id);
		request.setAttribute("orders", o);
		
		
		
		ticketOperation ticketOperation=new ticketOperation();
		
		//get a ticket
		Tickets tickets=ticketOperation.getAticket(ticket_id);
		request.setAttribute("ticket", tickets);
		
		//get ticket list
		ticketOperation gs=new ticketOperation();
		ArrayList<Tickets> al=gs.getAllTickets();
		request.setAttribute("tickets", al);
		
		//get unique ticket type
		ArrayList<Tickets> ticket_type = gs.getUniquTicket_type();
		request.setAttribute("unique_ticket_type", ticket_type);
		
		//get unique ticket name
		ArrayList<Tickets> ticket_name = gs.getUniquTicket_name();
		request.setAttribute("unique_ticket_name", ticket_name);
		
		
		if("modify".equals(flag))
			return mapping.findForward("modify");
		else if ("adminRead".equals(flag)) {
			return mapping.findForward("adminRead");
		}else if ("requestChange".equals(flag)) {
			return mapping.findForward("requestChange");
		}else if ("adminReadOnly".equals(flag)) {
			return mapping.findForward("adminReadOnly");
		}
		else
		{
			//get personal orders
			Orders orders = gb.getPersonalOrders(loginForm.getEmail());
			request.setAttribute("personalOrders", orders);
			return mapping.findForward("success");
		}
			
	}
}
