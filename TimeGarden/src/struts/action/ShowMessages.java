package struts.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.messageOperation;
import entity.Messages;
import struts.form.LoginForm;

public class ShowMessages extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		HttpSession session = request.getSession(true);
		LoginForm loginForm = (LoginForm)session.getAttribute("user");
		
		messageOperation mOperation = new messageOperation();
		ArrayList<Messages> messages = mOperation.getAllPersonalMessages(loginForm.getEmail());
		
		request.setAttribute("showMessages", messages);
		
		return mapping.findForward("showMessages");
	}

}
