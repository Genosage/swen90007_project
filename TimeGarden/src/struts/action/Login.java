package struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.LoginOperation;
import struts.form.LoginForm;

public class Login extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		// TODO Auto-generated method stub
		LoginForm loginForm = (LoginForm) form;
		
		LoginOperation loginOperation = new LoginOperation();
		if(loginOperation.islogin(loginForm)){
			HttpSession session=request.getSession(true);
			session.setAttribute("user",loginForm);
			return mapping.findForward("success");
		}
		else
			return mapping.findForward("fail");
	
	}
}
