package struts.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.userOperation;
import entity.User;

public class AdminAddTag extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		
		//get all users' email
		userOperation uOperation = new userOperation();
		ArrayList<User> au = uOperation.getAllUsersInfo();
		request.setAttribute("requests", au);
		return mapping.findForward("addTag");
	}
}
