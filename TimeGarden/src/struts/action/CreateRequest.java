package struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.requestOperation;
import struts.form.CreateRequestForm;
import struts.form.LoginForm;

public class CreateRequest extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		// TODO Auto-generated method stub
		CreateRequestForm createRequestForm = (CreateRequestForm) form;
		requestOperation ro = new requestOperation();
		
		
		//get user email
		HttpSession session = request.getSession(true);
		LoginForm loginForm = (LoginForm)session.getAttribute("user");
				
				
		if(ro.createRequest(createRequestForm, loginForm))
			return mapping.findForward("success");
		else
			return mapping.findForward("fail");
	
	}
}
