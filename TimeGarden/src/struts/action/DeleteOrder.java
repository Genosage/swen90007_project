package struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.orderOperation;
import DBOperation.requestOperation;

public class DeleteOrder extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub

		orderOperation mb=new orderOperation();
		String id=request.getParameter("order_id");
		
		requestOperation ro=new requestOperation();

		ro.deleteRequest(id);

			if(mb.deleteOrder(id))
				return mapping.findForward("success");
		return mapping.findForward("fail");
	}
}
