package struts.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.ticketOperation;
import entity.Tickets;


public class ShowTicketList extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		String flag=request.getParameter("flag");
		
		ticketOperation gs=new ticketOperation();
		ArrayList<Tickets> al=gs.getAllTickets();
		request.setAttribute("tickets", al);
		if ("requestChange".equals(flag)) 
			return mapping.findForward("requestChange");
		else if ("showOrderList".equals(flag)) {
			return mapping.findForward("showOrderList");
		}else if ("adminReadTicket".equals(flag)) {
			return mapping.findForward("adminReadTicket");
		}
		else
			return mapping.findForward("success");
	}
}
