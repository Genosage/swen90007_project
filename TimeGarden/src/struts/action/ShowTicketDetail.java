package struts.action;


import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.ticketOperation;
import DBOperation.userOperation;
import entity.Tickets;
import struts.form.LoginForm;

public class ShowTicketDetail extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(true);
		LoginForm loginForm = (LoginForm)session.getAttribute("user");
		userOperation uOperation = new userOperation();
		
		
		String id=request.getParameter("ticket_id");
		String flag=request.getParameter("flag");
		
		ticketOperation gb=new ticketOperation();
		Tickets t=gb.getAticket(id);
		request.setAttribute("tickets", t);
		
		ArrayList<Tickets> tickets = gb.getUniquTicket_name();
		request.setAttribute("unique_name", tickets);
		
		ArrayList<Tickets> ticket_type = gb.getUniquTicket_type();
		request.setAttribute("unique_type", ticket_type);
		
		if("buy".equals(flag))
		{
			int power = uOperation.getUserPower(loginForm.getEmail());
			request.setAttribute("power", power);
			return mapping.findForward("buy");
		}
		else if ("adminReadTicketDetail".equals(flag)) {
			return mapping.findForward("adminReadTicketDetail");
		}else if ("adminModifyTicket".equals(flag)) {
			return mapping.findForward("adminModifyTicket");
		}
		else
			return mapping.findForward("success");
	}
}
