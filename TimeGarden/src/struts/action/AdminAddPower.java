package struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.userOperation;

public class AdminAddPower extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String email = request.getParameter("email");
		System.out.println(email);
		userOperation uOperation = new userOperation();
		if (uOperation.updateUserPower(email)) {
			return mapping.findForward("addPower");
		}else {
			return mapping.findForward("fail");
		}
	}
}
