package struts.action;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.orderOperation;
import DBOperation.userOperation;
import entity.Orders;
import entity.User;


public class AdminIdentifyPowerUser extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		// TODO Auto-generated method stub
		
		//get all users' email
		userOperation uOperation = new userOperation();
		ArrayList<User> au = uOperation.getAllEmails();
		request.setAttribute("requests", au);
		
		
		//get each email and read corresponding orders
		Iterator<User> AllUserEmail = au.iterator();
		orderOperation operation = new orderOperation();
		
		//4 arraylist to store data
		ArrayList<Orders> allPowerOrders = new ArrayList<>();
		ArrayList<Orders> allOtherOrders = new ArrayList<>();
		ArrayList<Orders> allTaggedPowerOrders = new ArrayList<>();
		
		ArrayList<String> PowerUserEmail = new ArrayList<>();
		ArrayList<String> taggedPowerUserEmail = new ArrayList<>();
		ArrayList<String> UnPowerUserEmail = new ArrayList<>();
		
		int total_price = 0;
		
	
		while(AllUserEmail.hasNext()){
			User user = AllUserEmail.next();
			ArrayList<Orders> ao = operation.getAllPersonalOrders(user.getEmail());
			System.out.println(user.getEmail());
			
			//calculate total price
			Iterator<Orders> io = ao.iterator();
			while(io.hasNext()){
				
				for(int i = 0; i<ao.size(); i++)
				{
					total_price = total_price + io.next().getOrder_price();
				}
				System.out.println(total_price);
				
				int power = uOperation.getUserPower(user.getEmail());
				
				//encapsulate into an Arraylist
				if (total_price >= 1000 && power == 0) {
					total_price = 0;
					PowerUserEmail.add(user.getEmail());
					allPowerOrders.addAll(ao);
				}else if (total_price >= 1000 && power == 1) {
					total_price = 0;
					taggedPowerUserEmail.add(user.getEmail());
					allTaggedPowerOrders.addAll(ao);
				}else {
					total_price = 0;
					UnPowerUserEmail.add(user.getEmail());
					allOtherOrders.addAll(ao);
				}
			}
		}
		
		
        request.setAttribute("AllPowerUser", allPowerOrders);
        request.setAttribute("allPowerOrders", allPowerOrders);
        request.setAttribute("UnPowerUser", allOtherOrders);
        request.setAttribute("PowerUserEmail", PowerUserEmail);
        request.setAttribute("UnPowerUserEmail", UnPowerUserEmail);
        request.setAttribute("taggedPowerUserEmail", taggedPowerUserEmail);
		
        return mapping.findForward("AdminIdentifyPowerUser");

	
	}
}
