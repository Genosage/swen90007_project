package struts.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import DBOperation.userOperation;

public class AdminAddEachTag extends Action{
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response){
		String tag = request.getParameter("tag");
		String email = request.getParameter("email");
		userOperation uOperation = new userOperation();
		uOperation.updateUserTag(email, tag);
		return mapping.findForward("AdminAddEachTag");
	}
}
