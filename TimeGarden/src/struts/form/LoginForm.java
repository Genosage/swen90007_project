package struts.form;

import org.apache.struts.action.ActionForm;

public class LoginForm extends ActionForm{
	private String email;
	private Integer password;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getPassword() {
		return password;
	}
	public void setPassword(Integer password) {
		this.password = password;
	}
	
	
}
