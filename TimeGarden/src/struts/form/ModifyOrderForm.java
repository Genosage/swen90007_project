package struts.form;

import org.apache.struts.action.ActionForm;

public class ModifyOrderForm extends ActionForm{
	private Integer order_id;
	//private Integer ticket_id;
	private Integer order_price;
	private String email;
	private String newticket_name;
	private String newticket_type;
	private Integer newticket_number;
	private Integer order_status;
	public Integer getOrder_id() {
		return order_id;
	}
	public void setOrder_id(Integer order_id) {
		this.order_id = order_id;
	}
	/*public Integer getTicket_id() {
		return ticket_id;
	}
	public void setTicket_id(Integer ticket_id) {
		this.ticket_id = ticket_id;
	}*/
	public Integer getOrder_price() {
		return order_price;
	}
	public void setOrder_price(Integer order_price) {
		this.order_price = order_price;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNewticket_name() {
		return newticket_name;
	}
	public void setNewticket_name(String newticket_name) {
		this.newticket_name = newticket_name;
	}
	public String getNewticket_type() {
		return newticket_type;
	}
	public void setNewticket_type(String newticket_type) {
		this.newticket_type = newticket_type;
	}
	public Integer getNewticket_number() {
		return newticket_number;
	}
	public void setNewticket_number(Integer newticket_number) {
		this.newticket_number = newticket_number;
	}
	public Integer getOrder_status() {
		return order_status;
	}
	public void setOrder_status(Integer order_status) {
		this.order_status = order_status;
	}
	
	
}
