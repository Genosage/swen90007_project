package struts.form;

import org.apache.struts.action.ActionForm;

public class CreateRequestForm extends ActionForm{
	private Integer order_id;
	private String request_type;
	private String newticket_name;
	private String newticket_type;
	private Integer newticket_number;
	private Integer request_status;
	public Integer getOrder_id() {
		return order_id;
	}
	public void setOrder_id(Integer order_id) {
		this.order_id = order_id;
	}
	public String getRequest_type() {
		return request_type;
	}
	public void setRequest_type(String request_type) {
		this.request_type = request_type;
	}
	public String getNewticket_name() {
		return newticket_name;
	}
	public void setNewticket_name(String newticket_name) {
		this.newticket_name = newticket_name;
	}
	public String getNewticket_type() {
		return newticket_type;
	}
	public void setNewticket_type(String newticket_type) {
		this.newticket_type = newticket_type;
	}
	public Integer getNewticket_number() {
		return newticket_number;
	}
	public void setNewticket_number(Integer newticket_number) {
		this.newticket_number = newticket_number;
	}
	public Integer getRequest_status() {
		return request_status;
	}
	public void setRequest_status(Integer request_status) {
		this.request_status = request_status;
	}
	
	
}
