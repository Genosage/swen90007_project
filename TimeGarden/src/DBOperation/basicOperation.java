package DBOperation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class basicOperation {
	
		static ArrayList<Connection> list=new ArrayList<Connection>();

		public synchronized static  Connection getOneCon(){
			if(list.size()>0){
				return list.remove(0);
			}
			else{
				for(int i=0;i<5;i++){
					try {
						Class.forName("com.mysql.jdbc.Driver");
						Connection con=DriverManager.getConnection("jdbc:mysql://localhost/TG","root","Niki0405");
						list.add(con);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				return list.remove(0);
			}
		}
		
		public static void close(ResultSet rs){
			try {
				if(rs!=null)
					rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public static void close(PreparedStatement ps){
			try {
				if(ps!=null)
					ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public synchronized static void close(Connection con){
				if(con!=null)
					list.add(con);
		}
		
		
}
