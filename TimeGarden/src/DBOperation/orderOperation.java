package DBOperation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entity.Orders;
import struts.form.BuyTicketForm;
import struts.form.ModifyOrderForm;


public class orderOperation extends basicOperation{
	
public Orders getOrder(Integer id){
	Connection con=getOneCon();
	PreparedStatement pst=null;
	ResultSet rs=null;
	Orders o=new Orders();
		
		try {
			pst=con.prepareStatement("select * from orders where order_id = ?");
			pst.setInt(1, id);

			rs=pst.executeQuery();
			while(rs.next()){
				o.setOrder_id(rs.getInt(1));
				o.setTicket_id(rs.getInt(2));
				o.setTicket_number(rs.getInt(3));
				o.setOrder_price(rs.getInt(4));
				o.setEmail(rs.getString(5));
				o.setOrder_status(rs.getInt(6));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			close(rs);
			close(pst);
			close(con);
		}
		return o;
	}

public boolean addOrder(BuyTicketForm f)
{
	boolean b=false;
	PreparedStatement pst=null;
	Connection con=getOneCon();
	try {
		String sql="INSERT INTO orders value(null,?,?,?,?,?)";
		pst=con.prepareStatement(sql);
		pst.setInt(1, f.getTicket_id());
		pst.setInt(2, f.getTicket_number());
		pst.setInt(3, f.getOrder_price());
		pst.setString(4, f.getEmail());
		pst.setInt(5, 0);
		int n=pst.executeUpdate();
		if(n>0)
		{
			b=true;
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	finally
	{
		close(pst);
		close(con);
	}
	return b;
}

public ArrayList<Orders> getAllOrders(){
	
	ArrayList<Orders> al=new ArrayList<Orders>();
	
	Connection con=getOneCon();
	PreparedStatement pst=null;
	ResultSet rs=null;
	
	try {
		pst=con.prepareStatement("select * from orders");
		rs=pst.executeQuery();
		while(rs.next()){
			Orders o=new Orders();
	
			o.setOrder_id(rs.getInt(1));
			o.setTicket_id(rs.getInt(2));
			o.setTicket_number(rs.getInt(3));
			o.setOrder_price(rs.getInt(4));
			o.setEmail(rs.getString(5));
			o.setOrder_status(rs.getInt(6));
			al.add(o);
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally{
		close(rs);
		close(pst);
		close(con);
	}
	return al;
}

// get all personal orders;
public ArrayList<Orders> getAllPersonalOrders(String email){
	
	ArrayList<Orders> al=new ArrayList<Orders>();
	
	Connection con=getOneCon();
	PreparedStatement pst=null;
	ResultSet rs=null;
	
	try {
		pst=con.prepareStatement("select order_id,email,order_price from orders where email=?");
		pst.setString(1, email);
		rs=pst.executeQuery();
		while(rs.next()){
			Orders o=new Orders();
			o.setOrder_id(rs.getInt(1));
			o.setEmail(rs.getString(2));
			o.setOrder_price(rs.getInt(3));
			al.add(o);
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally{
		close(rs);
		close(pst);
		close(con);
	}
	return al;
}

public Orders getAnOrder(String id){
	Connection con=getOneCon();
	PreparedStatement pst=null;
	ResultSet rs=null;
	Orders o=new Orders();
	try {
		pst=con.prepareStatement("select * from orders where order_id=?");
		pst.setString(1, id);
		rs=pst.executeQuery();
		while(rs.next()){
			o.setOrder_id(rs.getInt(1));
			o.setTicket_id(rs.getInt(2));
			o.setTicket_number(rs.getInt(3));
			o.setOrder_price(rs.getInt(4));
			o.setEmail(rs.getString(5));
			o.setOrder_status(rs.getInt(6));
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally{
		close(rs);
		close(pst);
		close(con);
	}
	return o;
}
//get personal orders
public Orders getPersonalOrders(String email){
	Connection con=getOneCon();
	PreparedStatement pst=null;
	ResultSet rs=null;
	Orders o=new Orders();
	try {
		pst=con.prepareStatement("select * from orders where email=?");
		pst.setString(1, email);
		rs=pst.executeQuery();
		while(rs.next()){
			o.setOrder_id(rs.getInt(1));
			o.setTicket_id(rs.getInt(2));
			o.setTicket_number(rs.getInt(3));
			o.setOrder_price(rs.getInt(4));
			o.setEmail(rs.getString(5));
			o.setOrder_status(rs.getInt(6));
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally{
		close(rs);
		close(pst);
		close(con);
	}
	return o;
}
//modify orders
public boolean modifyOrder(ModifyOrderForm f, int id, int order_price)
{
	boolean b=false;
	PreparedStatement pst=null;
	Connection con=getOneCon();
	try {
		String sql="update orders set ticket_id =?,ticket_number=?,order_price=?,email=?, order_status=? where order_id=?";
		pst=con.prepareStatement(sql);

		pst.setInt(1, id);
		pst.setInt(2, f.getNewticket_number());
		pst.setInt(3, order_price);
		pst.setString(4, f.getEmail());
		pst.setInt(5, f.getOrder_status());
		pst.setInt(6, f.getOrder_id()); 
		int n=pst.executeUpdate();
		if(n>0)
		{
			b=true;
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	finally
	{
		close(pst);
		close(con);
	}
	return b;
}

public int getTicketId(String newTicket_name, String newTicket_type){
	Connection con=getOneCon();
	PreparedStatement pst=null;
	ResultSet rs=null;
	int t=0;
	//Tickets t=new Tickets();
	try {
		pst=con.prepareStatement("select ticket_id from ticket where name=? and ticket_type=?");
		pst.setString(1, newTicket_name);
		pst.setString(2, newTicket_type);
		rs=pst.executeQuery();
		while(rs.next()){
			
			t=rs.getInt(1);
			
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally{
		close(rs);
		close(pst);
		close(con);
	}
	return t;
}


public int getTicketPrice(int ticket_id){
	Connection con=getOneCon();
	PreparedStatement pst=null;
	ResultSet rs=null;
	int t=0;
	//Tickets t=new Tickets();
	try {
		pst=con.prepareStatement("select ticket_price from ticket where ticket_id=?");
		pst.setInt(1, ticket_id);
	
		rs=pst.executeQuery();
		while(rs.next()){
			
			t=rs.getInt(1);
			
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally{
		close(rs);
		close(pst);
		close(con);
	}
	return t;
}

public boolean deleteOrder(String id){
	boolean b=false;
	Connection con=(Connection) getOneCon();
	PreparedStatement pst=null;

	try {
		pst=con.prepareStatement("delete from orders where order_id=?");
		pst.setString(1, id);
		
		int i=pst.executeUpdate();
		if(i>0){
			b=true;
		}
		
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally{
		
		close(pst);
		close(con);
	}	
	return b;
	}

}
