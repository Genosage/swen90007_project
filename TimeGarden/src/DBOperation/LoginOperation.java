package DBOperation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import struts.form.LoginForm;
import struts.form.RegisterForm;

public class LoginOperation extends basicOperation{
	public boolean addUser(RegisterForm f)
	{
		boolean b=false;
		PreparedStatement pst=null;
		Connection con=getOneCon();
		try {
			String sql="INSERT INTO user value(null,?,?,0,0,0,0,null)";
			pst=con.prepareStatement(sql);
			pst.setString(1, f.getEmail());
			pst.setString(2, f.getPassword());
			
			int n=pst.executeUpdate();
			if(n>0)
			{
				b=true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			close(pst);
			close(con);
		}
		return b;
	}
	
	public boolean islogin(LoginForm u){
		boolean b=false;
		Connection con=getOneCon();
		PreparedStatement pst=null;
		ResultSet rs=null;
		try {
			String sql="SELECT email,password FROM user WHERE email=? and password=?";
			pst=con.prepareStatement(sql);
			pst.setString(1, u.getEmail());
			pst.setInt(2, u.getPassword());
			rs=pst.executeQuery();
			//�ж�����ֵ
			if(rs.next()){
				b=true;
			}else{
				b=false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
		    close(rs);
		    close(pst);
		    close(con);
		}
		return b;
		
	}

}
