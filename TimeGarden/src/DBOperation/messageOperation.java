package DBOperation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entity.Messages;
import entity.Orders;


public class messageOperation extends basicOperation{
	
	//add a new message
	public boolean addMessage(Integer ticket_id,String email)
	{
		boolean b=false;
		PreparedStatement pst=null;
		Connection con=getOneCon();
		try {
			String sql="INSERT INTO messages values(null,?,?,?)";
			pst=con.prepareStatement(sql);
			pst.setString(1, "Dear Customer, here's the new event which you may interested in");
			pst.setInt(2, ticket_id);
			pst.setString(3, email);
			int n=pst.executeUpdate();
			if(n>0)
			{
				b=true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			close(pst);
			close(con);
		}
		return b;
	}
	
	//get all personal messages;
	public ArrayList<Messages> getAllPersonalMessages(String email){
		Connection con=getOneCon();
		PreparedStatement pst=null;
		ResultSet rs=null;
		ArrayList<Messages> o=new ArrayList<>();
			
			try {
				pst=con.prepareStatement("select * from messages where email = ?");
				pst.setString(1, email);

				rs=pst.executeQuery();
				while(rs.next()){
					Messages messages = new Messages();
					messages.setMessage_id(rs.getInt(1));
					messages.setMessage_description(rs.getString(2));
					messages.setTicket_id(rs.getInt(3));
					messages.setEmail(rs.getString(4));
					o.add(messages);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				close(rs);
				close(pst);
				close(con);
			}
			return o;
		}

}
