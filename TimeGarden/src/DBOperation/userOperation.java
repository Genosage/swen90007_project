package DBOperation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entity.Orders;
import entity.Requests;
import entity.User;
import struts.form.BuyTicketForm;

public class userOperation extends basicOperation{
	
	public int getUserID(String email){
		Connection con=getOneCon();
		PreparedStatement pst=null;
		ResultSet rs=null;
		int id = -1;
		try {
			pst=con.prepareStatement("select user_id from user where email = ?");
			pst.setString(1, email);

			rs=pst.executeQuery();
			while(rs.next()){
				id = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			close(rs);
			close(pst);
			close(con);
		}
		return id;
	}
	//get user Power
	public int getUserPower(String email){
		Connection con=getOneCon();
		PreparedStatement pst=null;
		ResultSet rs=null;
		int id = -1;
		try {
			pst=con.prepareStatement("select power from user where email = ?");
			pst.setString(1, email);

			rs=pst.executeQuery();
			while(rs.next()){
				id = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			close(rs);
			close(pst);
			close(con);
		}
		return id;
	}
	
	//get user Tag
		public String getUserTag(String email){
			Connection con=getOneCon();
			PreparedStatement pst=null;
			ResultSet rs=null;
			String s = null;
			try {
				pst=con.prepareStatement("select tag from user where email = ?");
				pst.setString(1, email);

				rs=pst.executeQuery();
				while(rs.next()){
					s = rs.getString(1);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				close(rs);
				close(pst);
				close(con);
			}
			return s;
		}
	
	public User getUserDetail(String id){
		Connection con=getOneCon();
		PreparedStatement pst=null;
		ResultSet rs=null;
		User u=new User();
			
			try {
				pst=con.prepareStatement("select * from user where user_id = ?");
				pst.setString(1, id);

				rs=pst.executeQuery();
				while(rs.next()){
					u.setUser_id(rs.getInt(1));
					u.setEmail(rs.getString(2));
					u.setPassword(rs.getInt(3));
					u.setPower(rs.getInt(4));
					u.setHourticket_number(rs.getInt(5));
					u.setMinuteticket_number(rs.getInt(6));
					u.setSecondticket_number(rs.getInt(7));
					u.setTag(rs.getString(8));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				close(rs);
				close(pst);
				close(con);
			}
			return u;
		}
	
	//all user emails
	public ArrayList<User> getAllEmails(){
		
		ArrayList<User> al=new ArrayList<User>();
		
		Connection con=getOneCon();
		PreparedStatement pst=null;
		ResultSet rs=null;
		
		try {
			pst=con.prepareStatement("select email from user");
			rs=pst.executeQuery();
			while(rs.next()){
				User u=new User();
				u.setEmail(rs.getString(1));
			//System.out.println(rs.getString(1));
			
				al.add(u);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			close(rs);
			close(pst);
			close(con);
		}
		return al;
	}
	
	//select all user
		public ArrayList<User> getAllUsers(){
			
			ArrayList<User> al=new ArrayList<User>();
			
			Connection con=getOneCon();
			PreparedStatement pst=null;
			ResultSet rs=null;
			
			try {
				pst=con.prepareStatement("select user_id, email from user");
				rs=pst.executeQuery();
				while(rs.next()){
					User u=new User();
					u.setUser_id(rs.getInt(1));
					u.setEmail(rs.getString(2));
				System.out.println(rs.getString(1));
				
					al.add(u);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				close(rs);
				close(pst);
				close(con);
			}
			return al;
		}

		//select all user's email - tag pair
				public ArrayList<User> getAllUsersTag(){
					
					ArrayList<User> al=new ArrayList<User>();
					
					Connection con=getOneCon();
					PreparedStatement pst=null;
					ResultSet rs=null;
					
					try {
						pst=con.prepareStatement("select user_id,email,tag from user");
						rs=pst.executeQuery();
						while(rs.next()){
							User u=new User();
							u.setUser_id(rs.getInt(1));
							u.setEmail(rs.getString(2));
							u.setTag(rs.getString(3));
						//System.out.println(rs.getString(3));
						
							al.add(u);
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}finally{
						close(rs);
						close(pst);
						close(con);
					}
					return al;
				}
//update user Hour ticket number
		public boolean updateHourTicketNo(int ticket_number, String email){
			boolean b=false;
			Connection con=getOneCon();
			PreparedStatement pst=null;
			
			try {
				pst=con.prepareStatement("update user set" +
						" hourticket_number=hourticket_number+?" +
						" where email=?");
				pst.setInt(1, ticket_number);
				pst.setString(2, email);
				int i=pst.executeUpdate();
				if(i>0)
					b=true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				
				close(pst);
				close(con);
			}		
			return b;
		}
		
		//update user minute ticket number
				public boolean updateMinuteTicketNo(int ticket_number, String email){
					boolean b=false;
					Connection con=getOneCon();
					PreparedStatement pst=null;
					
					try {
						pst=con.prepareStatement("update user set" +
								" minuteticket_number=minuteticket_number+?" +
								" where email=?");
						pst.setInt(1, ticket_number);
						pst.setString(2, email);
						int i=pst.executeUpdate();
						if(i>0)
							b=true;
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}finally{
						
						close(pst);
						close(con);
					}		
					return b;
				}
				
				//update user minute ticket number
				public boolean updateSecondTicketNo(int ticket_number, String email){
					boolean b=false;
					Connection con=getOneCon();
					PreparedStatement pst=null;
					
					try {
						pst=con.prepareStatement("update user set" +
								" secondticket_number=secondticket_number+?" +
								" where email=?");
						pst.setInt(1, ticket_number);
						pst.setString(2, email);
						int i=pst.executeUpdate();
						if(i>0)
							b=true;
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}finally{
						
						close(pst);
						close(con);
					}		
					return b;
				}
				
	//update power;
	public boolean updateUserPower(String email){
		boolean b=false;	
		Connection con=getOneCon();
		PreparedStatement pst=null;			
		try {
			pst=con.prepareStatement("update user set" +
					" power=1" +
					" where email=?");
			pst.setString(1, email);
			int i=pst.executeUpdate();
			if(i>0)
				b=true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{	
			close(pst);
			close(con);
		}		
		return b;
	}
	
	//update tag;
		public boolean updateUserTag(String email,String tag){
			boolean b=false;	
			Connection con=getOneCon();
			PreparedStatement pst=null;			
			try {
				pst=con.prepareStatement("update user set" +
						" tag=?" +
						" where email=?");
				pst.setString(1, tag);
				pst.setString(2, email);
				int i=pst.executeUpdate();
				if(i>0)
					b=true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{	
				close(pst);
				close(con);
			}		
			return b;
		}
				
				//select all user
				public ArrayList<User> getAllUsersInfo(){
					
					ArrayList<User> al=new ArrayList<User>();
					
					Connection con=getOneCon();
					PreparedStatement pst=null;
					ResultSet rs=null;
					
					try {
						pst=con.prepareStatement("select email,hourticket_number,minuteticket_number,secondticket_number from user");
						rs=pst.executeQuery();
						while(rs.next()){
							User u=new User();
							u.setEmail(rs.getString(1));
							u.setHourticket_number(rs.getInt(2));
							u.setMinuteticket_number(rs.getInt(3));
							u.setSecondticket_number(rs.getInt(4));
							al.add(u);
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}finally{
						close(rs);
						close(pst);
						close(con);
					}
					return al;
				}
}
