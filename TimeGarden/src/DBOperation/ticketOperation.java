package DBOperation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entity.Tickets;
import struts.form.AddTicketsForm;
import struts.form.BuyTicketForm;
import struts.form.ModifyOrderForm;
import struts.form.ModifyTicketForm;

public class ticketOperation extends basicOperation{
public ArrayList<Tickets> getAllTickets(){
		
		ArrayList<Tickets> al=new ArrayList<Tickets>();
		
		Connection con=getOneCon();
		PreparedStatement pst=null;
		ResultSet rs=null;
		
		try {
			pst=con.prepareStatement("select * from ticket");
			rs=pst.executeQuery();
			while(rs.next()){
				Tickets g=new Tickets();
		
				g.setTicket_id(rs.getInt(1));
				g.setTicket_type(rs.getString(2));
				g.setTicket_price(rs.getInt(3));
				g.setName(rs.getString(4));
				g.setAddress(rs.getString(5));
				g.setImage(rs.getString(6));
				g.setDescription(rs.getString(7));
			
				al.add(g);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			close(rs);
			close(pst);
			close(con);
		}
		return al;
	}

public ArrayList<Tickets> getUniquTicket_type(){
	
	ArrayList<Tickets> al=new ArrayList<Tickets>();
	
	Connection con=getOneCon();
	PreparedStatement pst=null;
	ResultSet rs=null;
	
	try {
		pst=con.prepareStatement("select distinct ticket_type from ticket");
		rs=pst.executeQuery();
		while(rs.next()){
			Tickets g=new Tickets();
			g.setTicket_type(rs.getString(1));
			al.add(g);
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally{
		close(rs);
		close(pst);
		close(con);
	}
	return al;
}

public ArrayList<Tickets> getUniquTicket_name(){
	
	ArrayList<Tickets> al=new ArrayList<Tickets>();
	
	Connection con=getOneCon();
	PreparedStatement pst=null;
	ResultSet rs=null;
	
	try {
		pst=con.prepareStatement("select distinct name from ticket");
		rs=pst.executeQuery();
		while(rs.next()){
			Tickets g=new Tickets();
			g.setName(rs.getString(1));
			al.add(g);
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally{
		close(rs);
		close(pst);
		close(con);
	}
	return al;
}

public Tickets getAticket(String id){
	Connection con=getOneCon();
	PreparedStatement pst=null;
	ResultSet rs=null;
	Tickets t=new Tickets();
	try {
		pst=con.prepareStatement("select * from ticket where ticket_id=?");
		pst.setString(1, id);
		rs=pst.executeQuery();
		while(rs.next()){
			
			t.setTicket_id(rs.getInt(1));
			t.setTicket_type(rs.getString(2));
			t.setTicket_price(rs.getInt(3));
			t.setName(rs.getString(4));
			t.setAddress(rs.getString(5));
			t.setImage(rs.getString(6));
			t.setDescription(rs.getString(7));
			
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally{
		close(rs);
		close(pst);
		close(con);
	}
	return t;
}

public boolean addOrder(String ticket_name,Integer ticket_price,Integer purchase_num){
	boolean b=false;
	Connection con=getOneCon();
	PreparedStatement pst=null;
	
	try {
		pst=con.prepareStatement("insert into t_order_info values(null,?,?,?)");
		pst.setString(1, ticket_name);
		pst.setInt(2, ticket_price);
		pst.setInt(3, purchase_num);
		int i=pst.executeUpdate();
		if(i>0)
			b=true;
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally{
		
		close(pst);
		close(con);
	}		
	return b;
}

// -------------------------------------------- Tickets Management --------------------------------------- //
public boolean addTickets(AddTicketsForm f)
{
	boolean b=false;
	PreparedStatement pst=null;
	Connection con=getOneCon();
	try {
		String sql="INSERT INTO ticket value(?,?,?,?,?,?,?)";
		pst=con.prepareStatement(sql);
		pst.setInt(1, f.getTicket_id());
		pst.setString(2, f.getTicket_type());
		pst.setInt(3, f.getTicket_price());
		pst.setString(4, f.getName());
		pst.setString(5, f.getAddress());
		pst.setString(6, f.getImage());
		pst.setString(7, f.getDescription());
		int n=pst.executeUpdate();
		if(n>0)
		{
			b=true;
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	finally
	{
		close(pst);
		close(con);
	}
	return b;
}

//delete ticket
public boolean deleteTicket(String id){
	boolean b=false;
	Connection con=(Connection) getOneCon();
	PreparedStatement pst=null;

	try {
		pst=con.prepareStatement("delete from ticket where ticket_id=?");
		pst.setString(1, id);
		
		int i=pst.executeUpdate();
		if(i>0){
			b=true;
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}finally{
		
		close(pst);
		close(con);
	}	
	return b;
	}

//modify orders
public boolean modifyTicket(ModifyTicketForm f)
{
	boolean b=false;
	PreparedStatement pst=null;
	Connection con=getOneCon();
	try {
		String sql="update ticket set ticket_type=?, ticket_price=?,name=?,address=?, image=?, description=? where ticket_id=?";
		pst=con.prepareStatement(sql);

		pst.setString(1, f.getTicket_type());
		pst.setInt(2, f.getTicket_price());
		pst.setString(3, f.getName());
		pst.setString(4, f.getAddress());
		pst.setString(5, f.getImage());
		pst.setString(6, f.getDescription());
		pst.setInt(7, f.getTicket_id());
		
		int n=pst.executeUpdate();
		if(n>0)
		{
			b=true;
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	finally
	{
		close(pst);
		close(con);
	}
	return b;
}
}
