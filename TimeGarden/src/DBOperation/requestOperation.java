package DBOperation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entity.Orders;
import entity.Requests;
import struts.form.CreateRequestForm;
import struts.form.LoginForm;
import struts.form.ModifyOrderForm;

public class requestOperation extends basicOperation{
	
	public boolean createRequest(CreateRequestForm f, LoginForm lf)
	{
		boolean b=false;
		PreparedStatement pst=null;
		Connection con=getOneCon();
		try {
			String sql="INSERT INTO request value(null,?,?,?,?,?,?,?)";
			pst=con.prepareStatement(sql);
			pst.setInt(1, f.getOrder_id());
			pst.setString(2, f.getRequest_type());
			pst.setString(3, f.getNewticket_name());
			pst.setString(4, f.getNewticket_type());
			pst.setInt(5, f.getNewticket_number());
			pst.setInt(6, 0);
			pst.setString(7, lf.getEmail());

			int n=pst.executeUpdate();
			if(n>0)
			{
				b=true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			close(pst);
			close(con);
		}
		return b;
	}
	
	
	public ArrayList<Requests> getAllRequests(){
		
		ArrayList<Requests> al=new ArrayList<Requests>();
		
		Connection con=getOneCon();
		PreparedStatement pst=null;
		ResultSet rs=null;
		
		try {
			pst=con.prepareStatement("select * from request");
			rs=pst.executeQuery();
			while(rs.next()){
				Requests r=new Requests();
				
				r.setRequest_id(rs.getInt(1));
				r.setOrder_id(rs.getInt(2));
				r.setRequest_type(rs.getString(3));
				r.setNewticket_name(rs.getString(4));
				r.setNewticket_type(rs.getString(5));
				r.setNewticket_number(rs.getInt(6));
				r.setRequest_status(rs.getInt(7));
				r.setEmail(rs.getString(8));
				al.add(r);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			close(rs);
			close(pst);
			close(con);
		}
		return al;
	}
	
//get specific request
public ArrayList<Requests> getAllPersonalRequests(String email){
		
		ArrayList<Requests> al=new ArrayList<Requests>();
		
		Connection con=getOneCon();
		PreparedStatement pst=null;
		ResultSet rs=null;
		
		try {
			pst=con.prepareStatement("select * from request where email=?");
			pst.setString(1, email);
			rs=pst.executeQuery();
			while(rs.next()){
				Requests r=new Requests();
				
				r.setRequest_id(rs.getInt(1));
				r.setOrder_id(rs.getInt(2));
				r.setRequest_type(rs.getString(3));
				r.setNewticket_name(rs.getString(4));
				r.setNewticket_type(rs.getString(5));
				r.setNewticket_number(rs.getInt(6));
				r.setRequest_status(rs.getInt(7));
				r.setEmail(rs.getString(8));
				
				al.add(r);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			close(rs);
			close(pst);
			close(con);
		}
		return al;
	}
	
	public Requests getARequest(String id){
		Connection con=getOneCon();
		PreparedStatement pst=null;
		ResultSet rs=null;
		Requests r=new Requests();
		try {
			pst=con.prepareStatement("select order_id,request_type,newticket_name, newticket_type, newticket_number, request_status, email from request where request_id=?");
			pst.setString(1, id);
			rs=pst.executeQuery();
			while(rs.next()){
				r.setOrder_id(rs.getInt(1));
				r.setRequest_type(rs.getString(2));
				r.setNewticket_name(rs.getString(3));
				r.setNewticket_type(rs.getString(4));
				r.setNewticket_number(rs.getInt(5));
				r.setRequest_status(rs.getInt(6));
				r.setEmail(rs.getString(7));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			close(rs);
			close(pst);
			close(con);
		}
		return r;
	}
	
	//find ticket_id based on the order id
	public int getTicketId(int order_id){
		Connection con=getOneCon();
		PreparedStatement pst=null;
		ResultSet rs=null;
		int t=0;
		//Tickets t=new Tickets();
		try {
			pst=con.prepareStatement("select ticket_id from orders where order_id=?");
			pst.setInt(1, order_id);

			rs=pst.executeQuery();
			while(rs.next()){
				
				t=rs.getInt(1);
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			close(rs);
			close(pst);
			close(con);
		}
		return t;
	}

	public boolean deleteRequest(String id){
		boolean b=false;
		Connection con=(Connection) getOneCon();
		PreparedStatement pst=null;

		try {
			pst=con.prepareStatement("delete from request where order_id=?");
			pst.setString(1, id);
			
			int i=pst.executeUpdate();
			if(i>0){
				b=true;
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			
			close(pst);
			close(con);
		}	
		return b;
		}

}
